# 智诚物业管理系统
---
### 后端技术：
- MVC框架：Struts2
- ORM框架：MyBatis
- 整合框架：Spring
- 工作流引擎：Activiti Engine  

### 前端技术：
- MVVM：Vue.js
- jQuery
- B-JUI  

### 后台模板：
- FreeMarker  

### 数据库：
- MySQL
- Redis  

### 数据库连接池：
- Druid
